//
//  Login.h
//  SimpleChat
//
//  Created by Achim Sebastian on 11/12/15.
//  Copyright (c) 2015 All rights reserved.
//

#define Loading                                 @"Loading..."
#define Refresh                                 @"Refresh"

#define First_Tab_Nav_Title                     @"ONE"
#define Second_Tab_Nav_Title                    @"TWO"
#define Third_Tab_Nav_Title                     @"TREE"
#define Fourth_Tab_Nav_Title                    @"FOUR"
#define Fifth_Tab_Nav_Title                     @"FIVE"

#define First_Tab_Title                         @"Tab1"
#define Second_Tab_Title                        @"Tab2"
#define Third_Tab_Title                         @"Tab3"
#define Fourth_Tab_Title                        @"Tab4"
#define Fifth_Tab_Title                         @"Tab5"

#define First_Tab_Cell_SIZE                     50
#define First_Tab_Details_Cell_SIZE_Section1    100
#define First_Tab_Details_Cell_SIZE_Section2    30
#define First_Tab_Details_Cell_SIZE_Section3    60
#define First_Tab_Details_Cell_SIZE_Section4    60
#define First_Tab_Details_Cell_SIZE_Section5    30
#define First_Tab_Section_SIZE                  30

#define First_Tab_Section_1_Title               @"Change Text Here 1"
#define First_Tab_Section_2_Title               @"Change Text Here 2"
#define First_Tab_Section_3_Title               @"Change Text Here 3"
#define First_Tab_Section_4_Title               @"Change Text Here 4"
#define First_Tab_Section_5_Title               @"Change Text Here 5"
#define First_Tab_Section_6_Title               @"Change Text Here 6"

#define Share_Button_Title                      @"Share"

#define Mocked_Details_Data1                    @"dsf sdf sdf sdf sdf sd fsd fs dfs df sdf sd fs dfsdf dsf sdf sdf sdf sdf sd fsd fs dfs df sdf sd fs dfsdf"

#define Share_Notificatione_Name                @"Share_Data_Notification"

#define ActionSheet_Option1_Title               @"Option 1"
#define ActionSheet_Twitter_Title               @"Twitter"
#define ActionSheet_Facebook_Title              @"Facebook"
#define ActionSheet_LINE_Title                  @"LINE"
#define ActionSheet_Cancel_Title                @"Cancel"

#define Share_1                         @"Share1"
#define Share_2                         @"Share2"
#define Share_3                         @"Share3"
#define Share_4                         @"Share4"
#define Share_5                         @"Share5"
#define Share_6                         @"Share6"
#define Share_7                         @"Share7"
#define Share_8                         @"Share8"
#define Cancel                          @"Cancel"

#define Fifth_Tab_Section_1_Title               @"Change Text Here 1"
#define Fifth_Tab_Section_2_Title               @"Change Text Here 2"
#define Fifth_Tab_Section_3_Title               @"Change Text Here 3"

#define Fifth_Tab_Cell_1_Title               @"Change Text Cell 1"
#define Fifth_Tab_Cell_2_Title               @"Change Text Cell 2"
#define Fifth_Tab_Cell_3_Title               @"Change Text Cell 3"
#define Fifth_Tab_Cell_4_Title               @"Change Text Cell 4"
#define Fifth_Tab_Cell_5_Title               @"Change Text Cell 5"
#define Fifth_Tab_Cell_6_Title               @"Change Text Cell 6"

#define Email_Title              @"Email_Title"
#define Email_Body               @"Email_Body"
#define Email_Recipent           @"Email_Body@yahoo.com"

#define FifthTab_iTunesURL           @"https://itunes.apple.com/us/app/apple-store/id375380948?mt=8"
#define FifthTab_WebURL              @"http://www.google.com"

#define Share_1                         @"Share1"
#define Share_2                         @"Share2"
#define Share_3                         @"Share3"
#define Share_4                         @"Share4"
#define Share_5                         @"Share5"
#define Share_6                         @"Share6"
#define Share_7                         @"Share7"
#define Share_8                         @"Share8"
#define Cancel                          @"Cancel"

#define FithTab_Share_1                         @"Share1"
#define FithTab_Share_2                         @"Share2"
#define FithTab_Share_3                         @"Share3"
#define FithTab_Share_4                         @"Share4"
#define FithTab_Share_5                         @"Share5"
#define FithTab_Cancel                          @"Cancel"

#define First_Tab_Details_Section_1_Title               @"Details - Change Text Here 1"
#define First_Tab_Details_Section_2_Title               @"Details - Change Text Here 2"
#define First_Tab_Details_Section_3_Title               @"Details - Change Text Here 3"
#define First_Tab_Details_Section_4_Title               @"Details - Change Text Here 4"
#define First_Tab_Details_Section_5_Title               @"Details - Change Text Here 5"

