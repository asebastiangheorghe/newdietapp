//
//  Constants.h
//  Chat
//
//  Created by Achim Sebastian on 12/11/2015.
//  Copyright (c) 2014 YOUR_ORGANIZATION_NAME. All rights reserved.
//

typedef NS_ENUM(NSInteger, MessageType) {
    MessageTypeText,
    MessageTypeImage,
    MessageTypeStamp
};


#define HEXCOLOR(c) [UIColor colorWithRed:((float)((c & 0xFF0000) >> 16))/255.0 green:((float)((c & 0xFF00) >> 8))/255.0 blue:((float)(c & 0xFF))/255.0 alpha:1.0]

// RGB値でUIColorを取得
#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

// main color
#define setMainColor() [UIColor colorWithRed:255/255.0 green:172/255.0 blue:208/255.0 alpha:1.0]

// grey color
#define setGreyColor() [UIColor colorWithRed:180/255.0 green:180/255.0 blue:180/255.0 alpha:1.0]

// デバイス画面サイズを取得
#define FRAME_SIZE() [[UIScreen mainScreen] bounds].size
