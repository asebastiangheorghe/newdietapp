//
//  TutorialUtil.m
//  SimpleChat
//
//  Created by shoya on 8/14/15.
//  Copyright (c) 2015 binary, Inc. All rights reserved.
//

#import "TutorialUtil.h"
#import "Constants.h"
#import "DateUtil.h"
#import "Config.h"

@implementation TutorialUtil

+ (BOOL)isCompleteTutorial:(NSString*)key {
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

+ (void)checkCompleteTutorial:(NSString*)key TutorialAlertView:(CustomIOSAlertView*)tutorialAlertView contentView:(UIView*)contentView {
   
    contentView.layer.cornerRadius = 7.0f;
    contentView.layer.masksToBounds = YES;
    
    [tutorialAlertView setContainerView:contentView];
    [tutorialAlertView setButtonTitles:NULL];
    [tutorialAlertView show];
}

+ (void)checkEverydayPop:(CustomIOSAlertView*)tutorialAlertView contentView:(UIView*)contentView {
    
    NSUserDefaults *userDefalts = [NSUserDefaults standardUserDefaults];
    BOOL isCompleteMealTutorial = [userDefalts boolForKey:TUTORIAL_MEAL];
    
    NSString *key = [DateUtil getDateStringYYYYMMdd:0];
    BOOL isComplete = [userDefalts boolForKey:key];
    
    if (isCompleteMealTutorial == NO || isComplete == YES) {
        return;
    }

    NSInteger remainingCount = [DateUtil getRemainingDays];
    
    // end of thme term
    if (remainingCount < 0) {
        return;
    }
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, FRAME_SIZE().height * - 0.1, contentView.frame.size.width, FRAME_SIZE().height * 0.75)];
    backgroundImageView.image = [UIImage imageNamed:@"count_view"];
    [contentView addSubview:backgroundImageView];
    
    NSString *numberString;
    if (remainingCount < 10) {
       numberString = [[NSString stringWithFormat:@"%ld", (long)remainingCount] substringFromIndex:0];
    } else {
       numberString = [[NSString stringWithFormat:@"%ld", (long)remainingCount] substringFromIndex:1];
    }
    
    UIImageView *numberImageView = [self makeNumberImage:numberString contentView:contentView isOneDigit:YES];
    [contentView addSubview:numberImageView];
    
    contentView.alpha = 0.97;
    contentView.layer.cornerRadius = 7;
    contentView.backgroundColor = [UIColor whiteColor];
    
    /*
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentView.frame.size.height * 0.33, contentView.frame.size.width * 0.55, contentView.frame.size.height * 0.1)];
    descriptionLabel.text = @"今日の一言アドバイス！";
    descriptionLabel.textAlignment = NSTextAlignmentCenter;
    descriptionLabel.font = [UIFont systemFontOfSize:12];
    [contentView addSubview:descriptionLabel];
    */
     
    UILabel *everyWordLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentView.frame.size.height * 0.35, contentView.frame.size.width * 0.9, contentView.frame.size.height * 0.5)];
    everyWordLabel.center = CGPointMake(contentView.center.x, everyWordLabel.center.y);
    everyWordLabel.numberOfLines = 11;
    everyWordLabel.textAlignment = NSTextAlignmentLeft;
    everyWordLabel.textColor = [UIColor blackColor];
    everyWordLabel.alpha = 0.9;
    
    NSArray *oneWord = EVERY_WORD_ARRAY;
    everyWordLabel.text = [oneWord objectAtIndex:NUMBER_OF_REMAINING_DAYS_INITIAL_VALUE - remainingCount];
    [contentView addSubview:everyWordLabel];
    
    if (remainingCount > 9) {
        NSString *onesNumberString = [[NSString stringWithFormat:@"%ld", (long)remainingCount] substringToIndex:1];
        UIImageView *onesNumberImageView = [self makeNumberImage:onesNumberString contentView:contentView isOneDigit:NO];
        [contentView addSubview:onesNumberImageView];
    }
    
    tutorialAlertView.alpha = 0.95;
    tutorialAlertView.layer.masksToBounds = YES;
    
    [contentView bringSubviewToFront:[contentView viewWithTag:1]];
    
    [tutorialAlertView setContainerView:contentView];
    [tutorialAlertView setButtonTitles:NULL];
    [tutorialAlertView show];
    
    [userDefalts setBool:YES forKey:key];
    [userDefalts synchronize];
}

+ (UIImageView*)makeNumberImage:(NSString*)targetNumber contentView:(UIView*)contentView isOneDigit:(BOOL)isOneDigit {
    
    UIImageView *imageView;
    if (isOneDigit) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(contentView.frame.size.width * 0.54, contentView.frame.size.height * 0.23, contentView.frame.size.width * 0.15, contentView.frame.size.height * 0.15)];
    } else {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(contentView.frame.size.width * 0.40, contentView.frame.size.height * 0.23, contentView.frame.size.width * 0.15, contentView.frame.size.height * 0.15)];
    }
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-number.png", targetNumber]];
    return imageView;
}

+ (void)showTwitterShareView:(CustomIOSAlertView*)tutorialAlertView contentView:(UIView*)contentView {
   
    UIImageView *tweetShareImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height)];
    tweetShareImageView.image = [UIImage imageNamed:@"ExtensionTweet"];
    tweetShareImageView.layer.cornerRadius = 7;
    tweetShareImageView.layer.masksToBounds = YES;
    [contentView addSubview:tweetShareImageView];
    
    [tutorialAlertView setContainerView:contentView];
    [tutorialAlertView setButtonTitles:[NSMutableArray arrayWithObjects:@"シェアする", nil]];
    [tutorialAlertView show];
}

+ (void)showRegisterCompleteView:(CustomIOSAlertView*)alertView constentView:(UIView*)contentView {
    
    UIImageView *contentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 230, 350)];
    contentImageView.center = CGPointMake(contentView.center.x , contentImageView.center.y * 1.1);
    contentImageView.image = [UIImage imageNamed:@"completePopup"];
    contentImageView.layer.cornerRadius = 7;
    contentImageView.layer.masksToBounds = YES;
    [contentView addSubview:contentImageView];
    
    [alertView setContainerView:contentView];
    [alertView setButtonTitles:NULL];
    [alertView show];
}


@end