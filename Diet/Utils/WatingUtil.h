//
//  WatingUtil.h
//  SimpleChat
//
//  Created by shoya on 9/3/15.
//  Copyright (c) 2015 binary, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WatingUtil : NSObject

+ (NSInteger)generateRandomWatingCount:(NSInteger)baseCount;

@end
