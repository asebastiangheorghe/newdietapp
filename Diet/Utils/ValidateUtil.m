//
//  ValidateUtil.m
//  SimpleChat
//
//  Created by shoya on 8/25/15.
//  Copyright (c) 2015 binary, Inc. All rights reserved.
//

#import "ValidateUtil.h"

@implementation ValidateUtil

+ (void)checkNumberAndPointOnly:(NSString*)targetString {
    NSCharacterSet *digitCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    NSCharacterSet *stringCharacterSet = [NSCharacterSet characterSetWithCharactersInString:targetString];
    if ([digitCharacterSet isSupersetOfSet:stringCharacterSet]) {
        NSLog(@"数字のみ");
    } else {
        NSLog(@"数字以外の文字が存在");
    }
}

@end
