//
//  TutorialUtil.h
//  SimpleChat
//
//  Created by shoya on 8/14/15.
//  Copyright (c) 2015 binary, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomIOSAlertView.h"
#import <UIKit/UIKit.h>

@interface TutorialUtil : NSObject

+ (void)checkCompleteTutorial:(NSString*)key TutorialAlertView:(CustomIOSAlertView*)tutorialAlertView contentView:(UIView*)contentView;
+ (void)checkEverydayPop:(CustomIOSAlertView*)tutorialAlertView contentView:(UIView*)contentView;
+ (void)showTwitterShareView:(CustomIOSAlertView*)tutorialAlertView contentView:(UIView*)contentView;
+ (void)showRegisterCompleteView:(CustomIOSAlertView*)alertView constentView:(UIView*)contentView;
+ (BOOL)isCompleteTutorial:(NSString*)key;

@end
