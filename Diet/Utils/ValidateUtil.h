//
//  ValidateUtil.h
//  SimpleChat
//
//  Created by shoya on 8/25/15.
//  Copyright (c) 2015 binary, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidateUtil : NSObject

+ (void)checkNumberAndPointOnly:(NSString*)targetString;

@end
