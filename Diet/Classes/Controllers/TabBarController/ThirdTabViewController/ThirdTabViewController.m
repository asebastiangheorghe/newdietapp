//
//  ThirdTabViewController.m
//  Diet
//
//  Created by Sebastian Achim on 16/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "ThirdTabViewController.h"
#import "Config.h"
#import "ProgressHUD.h"
#import "SecondTabThirdTableViewCell.h"
#import "SecondTabReaderViewController.h"

@interface ThirdTabViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *datasourceDictionary;

@end

@implementation ThirdTabViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self setupDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.tabBarController.title = Third_Tab_Nav_Title;
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                   target:self
                                                                                   action:@selector(editData:)];
    self.tabBarController.navigationItem.rightBarButtonItem = editButton;
    
    [self setupDataSource];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.tabBarController.title = nil;
}

#pragma mark - Private methods

- (void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    CGFloat bottom =  self.tabBarController.tabBar.frame.size.height;
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, bottom, 0)];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.tableView];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"SecondTabThirdTableViewCell" bundle:nil] forCellReuseIdentifier:@"secondTabThirdTableViewCell"];
}

- (void)setupDataSource {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"save" ofType:@"plist"];
    self.datasourceDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ProgressHUD dismiss];
        [weakSelf.tableView reloadData];
    });
}

- (void)refreshData:(UIButton *)button {
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self setupDataSource];
}

- (void)editData:(UIButton *)button {
    if ([self.tableView isEditing]) {
        [self.tableView setEditing:NO animated:YES];
    } else {
        [self.tableView setEditing:YES animated:YES];
    }
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *key = self.datasourceDictionary.allKeys[indexPath.row];
    NSArray *arr = [key componentsSeparatedByString:@"|||"];
    
    SecondTabReaderViewController *details = [[SecondTabReaderViewController alloc] initWithSelectedUrlString:self.datasourceDictionary[key] titleString:arr[0] subtitleString:arr[1]];
    [self.navigationController pushViewController:details animated:YES];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datasourceDictionary.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"secondTabThirdTableViewCell";
    SecondTabThirdTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *key = self.datasourceDictionary.allKeys[indexPath.row];
    NSArray *arr = [key componentsSeparatedByString:@"|||"];
    NSString *title = arr[0];
    NSString *date = arr[1];
    
    NSDictionary *cellDict = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:title, date, nil]
                                                         forKeys:[NSArray arrayWithObjects:@"cell_title", @"cell_date", nil]];
    [cell updateCellWithData:cellDict];
    cell.imageView.image = [UIImage imageNamed:@"pin"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell setNeedsLayout];
    [cell setNeedsUpdateConstraints];
    [cell setNeedsDisplay];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return true;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [self.datasourceDictionary.allKeys objectAtIndex:indexPath.row];
    [self.datasourceDictionary removeObjectForKey:key];
    
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"save" ofType:@"plist"];
    
    if ([manager isWritableFileAtPath:plistPath]) {
        [self.datasourceDictionary writeToFile:plistPath atomically:NO];
        [manager setAttributes:[NSDictionary dictionaryWithObject:[NSDate date] forKey:NSFileModificationDate] ofItemAtPath:[[NSBundle mainBundle] bundlePath] error:nil];
    }
    
    [self.tableView setEditing:NO animated:NO];
    [self.tableView reloadData];
}

@end
