//
//  FirstTabTableViewCell.h
//  Diet
//
//  Created by Sebastian Achim on 18/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTabTableViewCell : UITableViewCell

- (void)updateCellWithData:(NSDictionary *)firstTabData;

@end
