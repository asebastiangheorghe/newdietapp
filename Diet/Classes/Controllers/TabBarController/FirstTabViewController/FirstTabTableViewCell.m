//
//  FirstTabTableViewCell.m
//  Diet
//
//  Created by Sebastian Achim on 18/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabTableViewCell.h"

@interface FirstTabTableViewCell ()

@property (nonatomic, strong) UIImageView   *firstTabCellImageView;
@property (nonatomic, strong) UILabel       *firstTabCellTitle;

@end

@implementation FirstTabTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        self.firstTabCellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5.0, 5.0, 40.0, 40.0)];
        [self addSubview:self.firstTabCellImageView];
        
        self.firstTabCellTitle = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 5.0, 250.0, 40.0)];
        [self addSubview:self.firstTabCellTitle];
    }
    
    return self;
}

#pragma mark - Cell Data Methods

- (void)updateCellWithData:(NSDictionary *)firstTabData {
    self.firstTabCellImageView.image = [UIImage imageNamed:[firstTabData objectForKey:@"cell_image"]];
    self.firstTabCellTitle.text = [firstTabData objectForKey:@"cell_title"];
}

@end
