//
//  FirstTabViewController.m
//  Diet
//
//  Created by Sebastian Achim on 16/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabViewController.h"
#import "Config.h"
#import "Constants.h"

#import "ProgressHUD.h"
#import "FirstTabTableViewCell.h"
#import "FirstTabDetailsView.h"

@interface FirstTabViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation FirstTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setupDataSource];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.tabBarController.navigationItem.leftBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.tabBarController.title = First_Tab_Nav_Title;
    
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                target:self
                                                                                action:@selector(editProfile:)];
    
    self.tabBarController.navigationItem.leftBarButtonItem = editButton;
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                   target:self
                                                                                   action:@selector(refreshData:)];
    self.tabBarController.navigationItem.rightBarButtonItem = refreshButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)setupUI {
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    [self.tableView registerClass:[FirstTabTableViewCell class] forCellReuseIdentifier:@"firstTabCellIdentifier"];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    CGFloat bottom =  self.tabBarController.tabBar.frame.size.height;
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, bottom, 0)];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
    
    [self.view addSubview:self.tableView];
}

- (void)setupDataSource {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FirstTabSectionsDataSource" ofType:@"plist"];
    self.dataSource = [NSMutableArray arrayWithContentsOfFile:plistPath];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ProgressHUD dismiss];
    });

}

#pragma mark - NavigationBar buttons actions

- (void)editProfile:(UIButton *)button {
    
}

- (void)refreshData:(UIButton *)button {
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self setupDataSource];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sectionArray = [self.dataSource objectAtIndex:section];
    
    return sectionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"firstTabCellIdentifier";
    FirstTabTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    NSDictionary *cellDict = [[self.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [cell updateCellWithData:cellDict];
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *cellDict = [[[self.dataSource objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"cell_data_dict"];
    
    FirstTabDetailsView *details = [[FirstTabDetailsView alloc] initWithSelectedRowData:cellDict];
    [self.navigationController pushViewController:details animated:YES];
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return First_Tab_Cell_SIZE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return First_Tab_Section_SIZE;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    [view setBackgroundColor:setMainColor()];

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    label.textColor = [UIColor whiteColor];
    
    switch (section) {
        case 0:
            [label setText:First_Tab_Section_1_Title];
            break;
        case 1:
            [label setText:First_Tab_Section_2_Title];
            break;
        case 2:
            [label setText:First_Tab_Section_3_Title];
            break;
        case 3:
            [label setText:First_Tab_Section_4_Title];
            break;
        case 4:
            [label setText:First_Tab_Section_5_Title];
            break;
        case 5:
            [label setText:First_Tab_Section_6_Title];
            break;
        default:
            break;
    }
    
    [view addSubview:label];
    
    return view;
}

@end

