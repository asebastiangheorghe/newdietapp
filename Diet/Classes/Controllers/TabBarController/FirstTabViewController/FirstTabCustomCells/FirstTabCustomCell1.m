//
//  FirstTabCustomCell1.m
//  Diet
//
//  Created by Sebastian Achim on 25/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabCustomCell1.h"
#import "Config.h"
#import "Constants.h"

@interface FirstTabCustomCell1 ()

@property (nonatomic, strong) IBOutlet UIImageView   *firstTabCellImageView;
@property (nonatomic, strong) IBOutlet UILabel       *firstTabCellTitle;
@property (nonatomic, strong) IBOutlet UIButton      *firstTabCellShareButton;

@end

@implementation FirstTabCustomCell1

- (void)awakeFromNib {
    // Initialization code
    
    self.firstTabCellTitle.textColor = setMainColor();
    
    [self.firstTabCellShareButton setTitle:Share_Button_Title forState:UIControlStateNormal];
    [self.firstTabCellShareButton addTarget:self
                                     action:@selector(share:)
                           forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - Cell Data Methods

- (void)updateCellWithData:(NSDictionary *)firstTabData {
    BOOL showShareButton = [[firstTabData objectForKey:@"cell_button_show"] boolValue];
    self.firstTabCellShareButton.hidden = !showShareButton;
    
    self.firstTabCellImageView.image = [UIImage imageNamed:@"notepad_cell_icon"];
    self.firstTabCellTitle.text = [firstTabData objectForKey:@"cellTitle"];
}

#pragma mark - Private methods

- (void)share:(UIButton *)button {
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Share_Notificatione_Name object:nil];
}

@end
