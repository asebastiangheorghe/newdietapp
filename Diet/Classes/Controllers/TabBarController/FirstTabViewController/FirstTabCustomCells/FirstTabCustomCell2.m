//
//  FirstTabCustomCell2.m
//  Diet
//
//  Created by Sebastian Achim on 25/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabCustomCell2.h"
#import "Config.h"
#import "Constants.h"

@interface FirstTabCustomCell2 ()




@end

@implementation FirstTabCustomCell2

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Cell Data Methods

- (void)updateCellWithData:(NSDictionary *)firstTabData {
    
    self.label1.text = firstTabData[@"cell_data1"];
    self.label2.text = firstTabData[@"cell_data2"];
    self.label3.text = firstTabData[@"cell_data3"];
}


@end
