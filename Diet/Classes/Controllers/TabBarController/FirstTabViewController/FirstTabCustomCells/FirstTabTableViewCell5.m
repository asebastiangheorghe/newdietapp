//
//  FirstTabTableViewCell5.m
//  Diet
//
//  Created by Alexandru-Mihai Lupulescu on 11/30/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabTableViewCell5.h"

@interface FirstTabTableViewCell5 ()

@property (weak, nonatomic) IBOutlet UILabel *leftTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTextLabel;

@end

@implementation FirstTabTableViewCell5

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithData:(NSDictionary *)firstTabData {
    self.leftTextLabel.text = firstTabData[@"cell_left_text"];
    self.rightTextLabel.text = firstTabData[@"cell_right_text"];
}

@end
