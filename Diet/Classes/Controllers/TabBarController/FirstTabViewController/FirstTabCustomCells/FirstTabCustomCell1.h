//
//  FirstTabCustomCell1.h
//  Diet
//
//  Created by Sebastian Achim on 25/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTabCustomCell1 : UITableViewCell

- (void)updateCellWithData:(NSDictionary *)firstTabData;

@end
