//
//  FirstTabTableViewCell4.m
//  Diet
//
//  Created by Alexandru-Mihai Lupulescu on 11/30/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabTableViewCell4.h"

@interface FirstTabTableViewCell4 ()

@property (weak, nonatomic) IBOutlet UIImageView *leadingImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation FirstTabTableViewCell4

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithData:(NSDictionary *)firstTabData {
    self.contentLabel.text = firstTabData[@"cell_content"];
    self.leadingImageView.image = [UIImage imageNamed:firstTabData[@"cell_image_name"]];
}

@end
