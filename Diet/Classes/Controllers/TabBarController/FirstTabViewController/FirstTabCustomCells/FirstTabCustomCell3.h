//
//  FirstTabCustomCell3.h
//  Diet
//
//  Created by Sebastian Achim on 25/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTabCustomCell3 : UITableViewCell

- (void)updateCellWithData:(NSDictionary *)firstTabData;

@end
