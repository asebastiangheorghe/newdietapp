//
//  FirstTabTableViewCell5.h
//  Diet
//
//  Created by Alexandru-Mihai Lupulescu on 11/30/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTabTableViewCell5 : UITableViewCell

- (void)updateCellWithData:(NSDictionary *)firstTabData;

@end
