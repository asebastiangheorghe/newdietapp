//
//  FirstTabCustomCell3.m
//  Diet
//
//  Created by Sebastian Achim on 25/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabCustomCell3.h"
#import "Config.h"
#import "Constants.h"

@interface FirstTabCustomCell3 ()

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation FirstTabCustomCell3

- (void)awakeFromNib {
    self.contentLabel.textColor = setMainColor();
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Cell Data Methods

- (void)updateCellWithData:(NSDictionary *)firstTabData {
    self.leftImageView.image = [UIImage imageNamed:firstTabData[@"cell_image"]];
    self.contentLabel.text = [firstTabData objectForKey:@"cell_text"];
}

@end
