//
//  FirstTabCustomCell2.h
//  Diet
//
//  Created by Sebastian Achim on 25/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTabCustomCell2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;

- (void)updateCellWithData:(NSDictionary *)firstTabData;

@end
