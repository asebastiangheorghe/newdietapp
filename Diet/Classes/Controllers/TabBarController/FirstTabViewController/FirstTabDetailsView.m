//
//  FirstTabDetailsView.m
//  Diet
//
//  Created by Sebastian Achim on 24/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FirstTabDetailsView.h"

#import "Config.h"
#import "Constants.h"

#import "FirstTabCustomCell1.h"
#import "FirstTabCustomCell2.h"
#import "FirstTabCustomCell3.h"
#import "FirstTabTableViewCell4.h"
#import "FirstTabTableViewCell5.h"

@interface FirstTabDetailsView () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation FirstTabDetailsView

- (id)initWithSelectedRowData:(NSDictionary *)selectedRowData {
    // Call the init method implemented by the superclass
    self = [super init];
    if (self) {
        self.dataSource = selectedRowData[@"cell_data_array"];
    }
    // Return the address of the new object
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareData:) name:Share_Notificatione_Name object:nil
     ];
        
    [self setupUI];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"FirstTabCustomCell1" bundle:nil] forCellReuseIdentifier:@"FirstTabCustomCell1"];

    [[self tableView] registerNib:[UINib nibWithNibName:@"FirstTabCustomCell2" bundle:nil] forCellReuseIdentifier:@"FirstTabCustomCell2"];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"FirstTabCustomCell3" bundle:nil] forCellReuseIdentifier:@"FirstTabCustomCell3"];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"FirstTabTableViewCell4" bundle:nil] forCellReuseIdentifier:@"FirstTabTableViewCell4"];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"FirstTabTableViewCell5" bundle:nil] forCellReuseIdentifier:@"FirstTabTableViewCell5"];
    
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    CGFloat bottom =  self.tabBarController.tabBar.frame.size.height;
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, bottom, 0)];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
    
    [self.view addSubview:self.tableView];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier1 = @"FirstTabCustomCell1";
    static NSString *cellIdentifier2 = @"FirstTabCustomCell2";
    static NSString *cellIdentifier3 = @"FirstTabCustomCell3";
    static NSString *cellIdentifier4 = @"FirstTabTableViewCell4";
    static NSString *cellIdentifier5 = @"FirstTabTableViewCell5";
    
    UITableViewCell *defaultCell;
    
    switch (indexPath.section) {
        case 0:
        {
            FirstTabCustomCell1 *cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1 forIndexPath:indexPath];
            
            NSDictionary *cellDict1 = [self.dataSource objectAtIndex:indexPath.section];
            [cell1 updateCellWithData:cellDict1];
            
            cell1.selectionStyle = UITableViewCellSelectionStyleNone;
            cell1.accessoryType = UITableViewCellAccessoryNone;
            
            defaultCell = cell1;
            return defaultCell;
        }
            break;
        case 1:
        {
            FirstTabCustomCell2 *cell2 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
            
            NSDictionary *cellDict2 = [self.dataSource objectAtIndex:indexPath.section];
            [cell2 updateCellWithData:cellDict2];
            
            cell2.selectionStyle = UITableViewCellSelectionStyleNone;
            cell2.accessoryType = UITableViewCellAccessoryNone;
            
            defaultCell = cell2;
            return defaultCell;
        }
            break;
        case 2:
        {
            FirstTabCustomCell3 *cell3 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3 forIndexPath:indexPath];
            
            NSDictionary *cellDict3 = [self.dataSource objectAtIndex:indexPath.section];
            [cell3 updateCellWithData:cellDict3];
            
            cell3.selectionStyle = UITableViewCellSelectionStyleNone;
            cell3.accessoryType = UITableViewCellAccessoryNone;
            
            defaultCell = cell3;
            return defaultCell;
        }
            break;
        case 3:
        {
            FirstTabTableViewCell4 *cell4 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier4 forIndexPath:indexPath];
            
            NSDictionary *cellDict4 = [self.dataSource objectAtIndex:indexPath.section];
            [cell4 updateCellWithData:cellDict4];
            
            cell4.selectionStyle = UITableViewCellSelectionStyleNone;
            cell4.accessoryType = UITableViewCellAccessoryNone;
            
            defaultCell = cell4;
            return defaultCell;
        }
            break;
        case 4:
        {
            FirstTabTableViewCell5 *cell5 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier5 forIndexPath:indexPath];
            NSDictionary *cellDict5 = [self.dataSource objectAtIndex:indexPath.section];
            [cell5 updateCellWithData:cellDict5];
            
            cell5.selectionStyle = UITableViewCellSelectionStyleNone;
            cell5.accessoryType = UITableViewCellAccessoryNone;
            
            defaultCell = cell5;
            return defaultCell;
        }
            break;
        default:
            break;
    }
    
    return defaultCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger cellHeight;
    
    switch (indexPath.section) {
        case 0:
            cellHeight = First_Tab_Details_Cell_SIZE_Section1;
            break;
            
        case 1:
            cellHeight = First_Tab_Details_Cell_SIZE_Section2;
            break;
            
        case 2:
            cellHeight = First_Tab_Details_Cell_SIZE_Section3;
            break;
            
        case 3:
            cellHeight = First_Tab_Details_Cell_SIZE_Section4;
            break;
            
        case 4:
            cellHeight = First_Tab_Details_Cell_SIZE_Section5;
            break;
            
        default:
            break;
    }
    
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return First_Tab_Section_SIZE;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    [view setBackgroundColor:setMainColor()];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    label.textColor = [UIColor whiteColor];
    
    switch (section) {
        case 0:
            [label setText:First_Tab_Details_Section_1_Title];
            break;
        case 1:
            [label setText:First_Tab_Details_Section_2_Title];
            break;
        case 2:
            [label setText:First_Tab_Details_Section_3_Title];
            break;
        case 3:
            [label setText:First_Tab_Details_Section_4_Title];
            break;
        case 4:
            [view setBackgroundColor:setGreyColor()];
            [label setText:First_Tab_Details_Section_5_Title];
            break;
        default:
            break;
    }
    
    [view addSubview:label];
    
    return view;
}

#pragma mark - NSNotifications

- (void)shareData:(NSNotification *)notif {
    UIAlertController *view = [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
   
    UIAlertAction *firstOption = [UIAlertAction
                         actionWithTitle:ActionSheet_Option1_Title
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             //Do some thing here
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];

    UIAlertAction *twitterOption = [UIAlertAction
                         actionWithTitle:ActionSheet_Twitter_Title
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             //Do some thing here
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];

    UIAlertAction *facebookOption = [UIAlertAction
                         actionWithTitle:ActionSheet_Facebook_Title
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             //Do some thing here
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];

    
    UIAlertAction *lineOption = [UIAlertAction
                         actionWithTitle:ActionSheet_LINE_Title
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action) {
                             //Do some thing here
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    UIAlertAction *cancelOption = [UIAlertAction
                             actionWithTitle:ActionSheet_Cancel_Title
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action) {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:firstOption];
    [view addAction:twitterOption];
    [view addAction:facebookOption];
    [view addAction:lineOption];
    [view addAction:cancelOption];
    
    [self presentViewController:view animated:YES completion:nil];
}

- (CGFloat)dynamicHeightForText:(NSString *)text font:(UIFont *)font {
    NSAttributedString *attributedText =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{NSFontAttributeName: font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){self.view.frame.size.width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    return rect.size.height;
}

@end
