//
//  FirstTabDetailsView.h
//  Diet
//
//  Created by Sebastian Achim on 24/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstTabDetailsView : UIViewController

- (id)initWithSelectedRowData:(NSDictionary *)selectedRowData;

@end
