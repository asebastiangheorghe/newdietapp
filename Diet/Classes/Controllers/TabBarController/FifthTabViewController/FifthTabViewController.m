//
//  FifthTabViewController.m
//  Diet
//
//  Created by Sebastian Achim on 16/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FifthTabViewController.h"
#import "Config.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>

@interface FifthTabViewController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UISwitch *switchView;

@end

@implementation FifthTabViewController

#pragma mark - Lifecycle methods.

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tabBarController.title = Fifth_Tab_Nav_Title;
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods.

- (void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    CGFloat bottom =  self.tabBarController.tabBar.frame.size.height;
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, bottom, 0)];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.tableView];
}

- (void)secondSectionFirstRowAction:(id)sender {
    // Email Subject
    NSString *emailTitle = Email_Title;
    // Email Content
    NSString *messageBody = Email_Body;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:Email_Recipent];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)secondSectionSecondRowAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: FifthTab_iTunesURL]];
}

- (void)secondSectionThirdRowAction:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:FithTab_Cancel destructiveButtonTitle:nil otherButtonTitles:
                            FithTab_Share_1,
                            FithTab_Share_2,
                            FithTab_Share_3,
                            FithTab_Share_4,
                            FithTab_Share_5,
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    popup.delegate = self;
}

- (void)thirdSectionFirstRowAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: FifthTab_WebURL]];
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 18;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [self secondSectionFirstRowAction:nil];
        } else if (indexPath.row == 1) {
            [self secondSectionSecondRowAction:nil];
        } else if (indexPath.row == 2) {
            [self secondSectionThirdRowAction:nil];
        }
    } else if (indexPath.section == 2) {
        [self thirdSectionFirstRowAction:nil];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"simpleCell";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    switch (indexPath.section) {
        case 0: {
            if (indexPath.row == 0) {
                    cell.textLabel.text = Fifth_Tab_Cell_1_Title;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
            } else {
                    cell.textLabel.text = Fifth_Tab_Cell_2_Title;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    self.switchView = [[UISwitch alloc] initWithFrame:CGRectMake(cell.frame.size.width - 60, 10, 40, cell.frame.size.height)];
                    [cell addSubview:self.switchView];
            }
            break;
        }
        case 1:
            if (indexPath.row == 0) {
                cell.textLabel.text = Fifth_Tab_Cell_3_Title;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                break;
            } else if (indexPath.row == 1) {
                cell.textLabel.text = Fifth_Tab_Cell_4_Title;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                break;
            } else {
                cell.textLabel.text = Fifth_Tab_Cell_5_Title;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleGray;
                break;
            }
        default:
            cell.textLabel.text = Fifth_Tab_Cell_6_Title;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            break;
    }
    
    
    [cell setNeedsLayout];
    [cell setNeedsUpdateConstraints];
    [cell setNeedsDisplay];
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    [view setBackgroundColor:setMainColor()];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    label.textColor = [UIColor whiteColor];
    
    switch (section) {
        case 0:
            [label setText:Fifth_Tab_Section_1_Title];
            break;
        case 1:
            [label setText:Fifth_Tab_Section_2_Title];
            break;
        case 2:
            [label setText:Fifth_Tab_Section_3_Title];
            break;
        default:
            break;
    }
    
    [view addSubview:label];
    
    return view;
}

#pragma mark - MFMailComposeViewControllerDelegate methods.

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case 1: {
            switch (buttonIndex) {
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

@end
