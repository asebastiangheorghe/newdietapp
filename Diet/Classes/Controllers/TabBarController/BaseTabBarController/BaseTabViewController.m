//
//  BaseTabViewController.m
//  Diet
//
//  Created by Sebastian Achim on 16/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "BaseTabViewController.h"
#import "Config.h"
#import "Constants.h"

#import "FirstTabViewController.h"
#import "SecondTabViewController.h"
#import "ThirdTabViewController.h"
#import "FourthTabViewController.h"
#import "FifthTabViewController.h"

@interface BaseTabViewController () <UITabBarControllerDelegate>

@end

@implementation BaseTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    NSMutableArray *localViewControllersArray = [[NSMutableArray alloc] initWithCapacity:3];
    
    FirstTabViewController *firstTabController = [[FirstTabViewController alloc] init];
    firstTabController.tabBarItem.title = First_Tab_Title;
    firstTabController.tabBarItem.image = [UIImage imageNamed:@"notes_icon"];
    
    SecondTabViewController *secondTabController = [[SecondTabViewController alloc] init];
    secondTabController.tabBarItem.title = Second_Tab_Title;
    secondTabController.tabBarItem.image = [UIImage imageNamed:@"globe_icon"];
    
    ThirdTabViewController *thirdTabController = [[ThirdTabViewController alloc] init];
    thirdTabController.tabBarItem.title = Third_Tab_Title;
    thirdTabController.tabBarItem.image = [UIImage imageNamed:@"pin_icon"];

    FourthTabViewController *fourthTabController = [[FourthTabViewController alloc] init];
    fourthTabController.tabBarItem.title = Fourth_Tab_Title;
    fourthTabController.tabBarItem.image = [UIImage imageNamed:@"light_icon"];
    
    FifthTabViewController *fifthTabController = [[FifthTabViewController alloc] init];
    fifthTabController.tabBarItem.title = Fifth_Tab_Title;
    fifthTabController.tabBarItem.image = [UIImage imageNamed:@"settings_icon"];
    
    [localViewControllersArray addObject:firstTabController];
    [localViewControllersArray addObject:secondTabController];
    [localViewControllersArray addObject:thirdTabController];
    [localViewControllersArray addObject:fourthTabController];
    [localViewControllersArray addObject:fifthTabController];
    
    self.viewControllers = localViewControllersArray;
    self.view.autoresizingMask = (UIViewAutoresizingFlexibleHeight);
    
    self.selectedIndex = 0;
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    return YES;
}

@end
