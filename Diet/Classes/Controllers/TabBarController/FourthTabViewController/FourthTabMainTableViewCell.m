//
//  FourthTabMainTableViewCell.m
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "FourthTabMainTableViewCell.h"

@implementation FourthTabMainTableViewCell

#pragma mark - Lifecycle methods

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public methods

- (void)updateCellWithData:(NSDictionary *)data {
    self.subtitleLabel.text = [data objectForKey:@"cell_subtitle"];
    self.titleLabel.text = [data objectForKey:@"cell_title"];
    self.leftImageView.image = [UIImage imageNamed:[data objectForKey:@"cell_image"]];
}

@end
