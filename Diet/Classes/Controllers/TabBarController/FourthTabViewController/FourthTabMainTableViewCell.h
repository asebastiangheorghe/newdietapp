//
//  FourthTabMainTableViewCell.h
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FourthTabMainTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

- (void)updateCellWithData:(NSDictionary *)data;

@end
