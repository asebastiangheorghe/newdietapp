//
//  SecondTabDetailedViewController.m
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "SecondTabDetailedViewController.h"
#import "Config.h"
#import "SecondTabMainTableViewCell.h"
#import "SecondTabSecondaryTableViewCell.h"
#import "SecondTabThirdTableViewCell.h"
#import "ProgressHUD.h"

@interface SecondTabDetailedViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *selectedRowData;
@property (nonatomic, strong) NSString *titleString;

@end

@implementation SecondTabDetailedViewController

#pragma mark - Lifecyle methods

- (id)initWithSelectedRowData:(NSArray *)selectedRowData titleString:(NSString *)titleString {
    self = [super init];
    
    if (self) {
        self.selectedRowData = selectedRowData;
        self.titleString = titleString;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.backItem.title = @"";
    
    [self setupUI];
    [self setupDataSource];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.topItem.title = self.titleString;

    UIBarButtonItem *button = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                               target:self
                               action:@selector(refreshData:)];
    self.navigationItem.rightBarButtonItem = button;
    
    self.navigationController.navigationBarHidden = false;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Private methods

- (void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    CGFloat bottom =  self.tabBarController.tabBar.frame.size.height;
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, bottom, 0)];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.tableView];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"SecondTabSecondaryTableViewCell" bundle:nil] forCellReuseIdentifier:@"secondTabSecondaryTableViewCell"];
}

- (void)setupDataSource {
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ProgressHUD dismiss];
        [weakSelf.tableView reloadData];
    });
}

- (void)refreshData:(UIButton *)button {
    [ProgressHUD show:@"Loading..." Interaction:NO];
    
    [self setupDataSource];
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == self.selectedRowData.count) {
        [self refreshData:nil];
    } else {
        NSDictionary *dict = [self.selectedRowData  objectAtIndex:indexPath.row];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: dict[@"cell_url"]]];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.selectedRowData count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [self.selectedRowData  count]) {
        UITableViewCell *tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        tableViewCell.textLabel.text = @"Refresh";
        tableViewCell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        return tableViewCell;
    } else {
        static NSString *cellIdentifier = @"secondTabSecondaryTableViewCell";
        SecondTabSecondaryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        NSDictionary *cellDict = [self.selectedRowData objectAtIndex:indexPath.row];
        [cell updateCellWithData:cellDict];
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        [cell setNeedsLayout];
        [cell setNeedsUpdateConstraints];
        [cell setNeedsDisplay];
        
        return cell;
    }
}

@end

