//
//  SecondTabReaderViewController.m
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "SecondTabReaderViewController.h"
#import "Config.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

@interface SecondTabReaderViewController () <UIWebViewDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) NSString *subtitleString;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIView *bottomBar;

@property (nonatomic, strong) UIButton *backNavigationButton;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *forwardButton;
@property (nonatomic, strong) UIButton *refreshButton;
@property (nonatomic, strong) UIButton *pinButton;
@property (nonatomic, strong) UIButton *shareButton;

@end

@implementation SecondTabReaderViewController

#pragma mark - Lifecycle methods

- (id)initWithSelectedUrlString:(NSString *)urlString titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString {
    self = [super init];
    
    if (self) {
        self.urlString = urlString;
        self.titleString = titleString;
        self.subtitleString = subtitleString;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.bottomBar = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.bottomBar];
    self.bottomBar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomBar
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomBar
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomBar
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomBar
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:0
                                                           constant:50]];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.bottomBar
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.webView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    self.webView.delegate = self;
    
    self.backNavigationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar addSubview:self.backNavigationButton];
    self.backNavigationButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backNavigationButton
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0 / 6.0
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backNavigationButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeLeading
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backNavigationButton
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backNavigationButton
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1
                                                                constant:0]];
    [self.backNavigationButton addTarget:self action:@selector(backNavigationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.backNavigationButton setImage:[UIImage imageNamed:@"backArrow"] forState:UIControlStateNormal];
    
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar addSubview:self.backButton];
    self.backButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backButton
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0 / 6.0
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.backNavigationButton
                                                               attribute:NSLayoutAttributeTrailing
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backButton
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.backButton
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1
                                                                constant:0]];
    [self.backButton addTarget:self.webView action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setImage:[UIImage imageNamed:@"back1"] forState:UIControlStateNormal];
    
    self.forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar addSubview:self.forwardButton];
    self.forwardButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0 / 6.0
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.backButton
                                                               attribute:NSLayoutAttributeTrailing
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.forwardButton
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1
                                                                constant:0]];
    [self.forwardButton addTarget:self.webView action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
    [self.forwardButton setImage:[UIImage imageNamed:@"forward1"] forState:UIControlStateNormal];
    
    self.refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar addSubview:self.refreshButton];
    self.refreshButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.refreshButton
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0 / 6.0
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.refreshButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.forwardButton
                                                               attribute:NSLayoutAttributeTrailing
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.refreshButton
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.refreshButton
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1
                                                                constant:0]];
    [self.refreshButton addTarget:self action:@selector(refreshButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.refreshButton setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    
    self.pinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar addSubview:self.pinButton];
    self.pinButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.pinButton
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0 / 6.0
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.pinButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.refreshButton
                                                               attribute:NSLayoutAttributeTrailing
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.pinButton
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.pinButton
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1
                                                                constant:0]];
    [self.pinButton addTarget:self action:@selector(pinButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.pinButton setImage:[UIImage imageNamed:@"pin"] forState:UIControlStateNormal];
    
    self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.bottomBar addSubview:self.shareButton];
    self.shareButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.shareButton
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0 / 6.0
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.shareButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.pinButton
                                                               attribute:NSLayoutAttributeTrailing
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.shareButton
                                                               attribute:NSLayoutAttributeCenterY
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeCenterY
                                                              multiplier:1
                                                                constant:0]];
    [self.bottomBar addConstraint:[NSLayoutConstraint constraintWithItem:self.shareButton
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.bottomBar
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1
                                                                constant:0]];
    [self.shareButton addTarget:self action:@selector(shareButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBarHidden = true;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)backNavigationButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)refreshButtonAction:(id)sender {
    [self.webView reload];
}

- (void)pinButtonAction:(id)sender {
    NSFileManager* manager = [NSFileManager defaultManager];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"save" ofType:@"plist"];
    
    if ([manager isWritableFileAtPath:plistPath])
    {
        NSMutableDictionary* infoDict = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        [infoDict setObject:self.urlString forKey:[NSString stringWithFormat:@"%@|||%@", self.titleString, self.subtitleString]];
        [infoDict writeToFile:plistPath atomically:NO];
        [manager setAttributes:[NSDictionary dictionaryWithObject:[NSDate date] forKey:NSFileModificationDate] ofItemAtPath:[[NSBundle mainBundle] bundlePath] error:nil];
    }
    self.pinButton.enabled = false;
}

- (void)shareButtonAction:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:Cancel destructiveButtonTitle:nil otherButtonTitles:
                            Share_1,
                            Share_2,
                            Share_3,
                            Share_4,
                            Share_5,
                            Share_6,
                            Share_7,
                            Share_8,
                            nil];
    popup.tag = 1;
    [popup showInView:self.view];
    popup.delegate = self;
}

#pragma mark - UIWebViewDelegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return true;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error {
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
}

#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    
    
    switch (buttonIndex) {
        case 0: {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = self.urlString;
        }
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.urlString]];
            break;
        case 2: {
            NSString *emailTitle = self.titleString;
            NSString *messageBody = self.urlString;
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailTitle];
            [mc setMessageBody:messageBody isHTML:NO];
            
            // Present mail view controller on screen
            [self presentViewController:mc animated:YES completion:NULL];
        }
            break;
        case 3: {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:self.urlString];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
            break;
        case 4: {
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            content.contentURL = [NSURL URLWithString:self.urlString];
            FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
            dialog.mode = FBSDKShareDialogModeFeedWeb;
            dialog.shareContent = content;
            dialog.fromViewController = self;
            [dialog show];
        }
            break;
        default:
            break;
    }
    
}

#pragma mark - MFMailComposeViewControllerDelegate methods

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
