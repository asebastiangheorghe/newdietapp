//
//  SecondTabViewController.m
//  Diet
//
//  Created by Sebastian Achim on 16/11/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "SecondTabViewController.h"
#import "Config.h"
#import "Constants.h"
#import "SecondTabMainTableViewCell.h"
#import "SecondTabSecondaryTableViewCell.h"
#import "SecondTabThirdTableViewCell.h"
#import "ProgressHUD.h"
#import "SecondTabDetailedViewController.h"
#import "SecondTabReaderViewController.h"

@interface SecondTabViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UISegmentedControl *topSegmentedControl;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *datasourceDictionary;
@property (nonatomic, strong) NSArray *segItemsArray;

@end

@implementation SecondTabViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.segItemsArray = [NSArray arrayWithObjects: @"section1", @"section2", @"section3", nil];
    self.topSegmentedControl = [[UISegmentedControl alloc] initWithItems:self.segItemsArray];
    self.topSegmentedControl.frame = CGRectMake(0, 0, 200, 30);
    self.topSegmentedControl.selectedSegmentIndex = 0;
    self.topSegmentedControl.tintColor = setMainColor();
    
    self.tabBarController.title = @"";
    
    [self setupUI];
    [self setupDataSource];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.tabBarController.navigationItem.titleView = self.topSegmentedControl;
    [self.topSegmentedControl addTarget:self action:@selector(topSegmentedControlAction:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                   target:self
                                                                                   action:@selector(refreshData:)];
    self.tabBarController.navigationItem.rightBarButtonItem = refreshButton;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.tabBarController.navigationItem.titleView = nil;
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)topSegmentedControlAction:(id)sender {
    [self refreshData:nil];
}

- (void)setupUI {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    CGFloat bottom =  self.tabBarController.tabBar.frame.size.height;
    [self.tableView setScrollIndicatorInsets:UIEdgeInsetsMake(0, 0, bottom, 0)];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, bottom, 0);
    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.tableView];
    
    [[self tableView] registerNib:[UINib nibWithNibName:@"SecondTabMainTableViewCell" bundle:nil] forCellReuseIdentifier:@"secondTabMainTableViewCell"];
    [[self tableView] registerNib:[UINib nibWithNibName:@"SecondTabSecondaryTableViewCell" bundle:nil] forCellReuseIdentifier:@"secondTabSecondaryTableViewCell"];
    [[self tableView] registerNib:[UINib nibWithNibName:@"SecondTabThirdTableViewCell" bundle:nil] forCellReuseIdentifier:@"secondTabThirdTableViewCell"];
}

- (void)setupDataSource {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"SecondTabSectionsDataSource" ofType:@"plist"];
    self.datasourceDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ProgressHUD dismiss];
        [weakSelf.tableView reloadData];
    });
}

- (void)refreshData:(UIButton *)button {
    [ProgressHUD show:Loading Interaction:NO];
    
    [self setupDataSource];
}

#pragma mark - UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.topSegmentedControl.selectedSegmentIndex == 0) {
        return 75;
    } else if (self.topSegmentedControl.selectedSegmentIndex == 1) {
        return 75;
    }
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.topSegmentedControl.selectedSegmentIndex == 0) {
        if (indexPath.row == [[self.datasourceDictionary objectForKey:self.segItemsArray[0]] count]) {
            [self refreshData:nil];
        } else {
            NSDictionary *dict = [[self.datasourceDictionary objectForKey:self.segItemsArray[0]] objectAtIndex:indexPath.row];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: dict[@"cell_url"]]];
        }
        
    } else if (self.topSegmentedControl.selectedSegmentIndex == 1) {
        if (indexPath.row == [[self.datasourceDictionary objectForKey:self.segItemsArray[1]] count]) {
            [self refreshData:nil];
        } else {
            NSDictionary *dict = [[self.datasourceDictionary objectForKey:self.segItemsArray[1]] objectAtIndex:indexPath.row];
            
            SecondTabReaderViewController *details = [[SecondTabReaderViewController alloc] initWithSelectedUrlString:dict[@"cell_url"] titleString:dict[@"cell_text"] subtitleString:dict[@"cell_date"]];
            [self.navigationController pushViewController:details animated:NO];
        }
    } else {
        NSDictionary *dict = [[self.datasourceDictionary objectForKey:self.segItemsArray[2]] objectAtIndex:indexPath.row];
        SecondTabDetailedViewController *details = [[SecondTabDetailedViewController alloc] initWithSelectedRowData:dict[@"cell_data"] titleString:dict[@"cell_title"]];
        [self.navigationController pushViewController:details animated:NO];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.topSegmentedControl.selectedSegmentIndex == 0) {
        return [[self.datasourceDictionary objectForKey:self.segItemsArray[0]] count] + 1;
    } else if (self.topSegmentedControl.selectedSegmentIndex == 1) {
        return [[self.datasourceDictionary objectForKey:self.segItemsArray[1]] count] + 1;
    }
    return [[self.datasourceDictionary objectForKey:self.segItemsArray[2]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.topSegmentedControl.selectedSegmentIndex == 0) {
        if (indexPath.row == [[self.datasourceDictionary objectForKey:self.segItemsArray[0]] count]) {
            UITableViewCell *tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            tableViewCell.textLabel.textColor = setMainColor();
            tableViewCell.textLabel.text = Refresh;
            tableViewCell.textLabel.textAlignment = NSTextAlignmentCenter;
            
            return tableViewCell;
        } else {
            static NSString *cellIdentifier = @"secondTabMainTableViewCell";
            SecondTabMainTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            
            NSDictionary *cellDict = [[self.datasourceDictionary objectForKey:self.segItemsArray[0]] objectAtIndex:indexPath.row];
            [cell updateCellWithData:cellDict];
            cell.numberLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            [cell setNeedsLayout];
            [cell setNeedsUpdateConstraints];
            [cell setNeedsDisplay];
            
            return cell;
        }
    } else if (self.topSegmentedControl.selectedSegmentIndex == 1) {
        if (indexPath.row == [[self.datasourceDictionary objectForKey:self.segItemsArray[1]] count]) {
            UITableViewCell *tableViewCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
            tableViewCell.textLabel.textColor = setMainColor();
            tableViewCell.textLabel.text = Refresh;
            tableViewCell.textLabel.textAlignment = NSTextAlignmentCenter;
            
            return tableViewCell;
        } else {
            static NSString *cellIdentifier = @"secondTabSecondaryTableViewCell";
            SecondTabSecondaryTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
            
            NSDictionary *cellDict = [[self.datasourceDictionary objectForKey:self.segItemsArray[1]] objectAtIndex:indexPath.row];
            [cell updateCellWithData:cellDict];
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
            return cell;
        }
    } else {
        static NSString *cellIdentifier = @"secondTabThirdTableViewCell";
        SecondTabThirdTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        NSDictionary *cellDict = [[self.datasourceDictionary objectForKey:self.segItemsArray[2]] objectAtIndex:indexPath.row];
        [cell updateCellWithData:cellDict];
        
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

@end
