//
//  SecondTabDetailedViewController.h
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTabDetailedViewController : UIViewController

- (id)initWithSelectedRowData:(NSArray *)selectedRowData titleString:(NSString *)titleString;

@end
