//
//  SecondTabSecondaryTableViewCell.m
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import "SecondTabSecondaryTableViewCell.h"

@implementation SecondTabSecondaryTableViewCell

#pragma mark - Lifecycle methods

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public methods

- (void)updateCellWithData:(NSDictionary *)data {
    if ([data objectForKey:@"cell_image"] != nil) {
        self.trailingImageView.image = [UIImage imageNamed:[data objectForKey:@"cell_image"]];
        self.trailingConstraint.constant = -3;
    } else {
        self.trailingConstraint.constant = -78;
    }
    
    self.dateLabel.text = [data objectForKey:@"cell_date"];
    self.titleLabel.text = [data objectForKey:@"cell_text"];
    self.noteLabel.text = [data objectForKey:@"cell_note"];
    self.subtitleLabel.text = [data objectForKey:@"cell_subtitle"];
}

@end
