//
//  SecondTabSecondaryTableViewCell.h
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTabSecondaryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *trailingImageView;

- (void)updateCellWithData:(NSDictionary *)data;

@end
