//
//  SecondTabMainTableViewCell.h
//  Diet
//
//  Created by Sebastian Achim on 11/28/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTabMainTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *trailingImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingConstraint;

- (void)updateCellWithData:(NSDictionary *)data;

@end
