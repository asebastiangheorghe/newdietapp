//
//  SecondTabReaderViewController.h
//  Diet
//
//  Created by Sebastian Achim on 11/29/15.
//  Copyright © 2015 Tomohiro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTabReaderViewController : UIViewController

- (id)initWithSelectedUrlString:(NSString *)urlString titleString:(NSString *)titleString subtitleString:(NSString *)subtitleString;

@end
