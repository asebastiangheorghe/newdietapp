//
//  PFFile+NSCoding.m
//  UpdateZen
//
//  Created by Martin Rybak on 2/3/14.
//  Copyright (c) 2014 UpdateZen. All rights reserved.
//

#import "PFFile+NSCoding.h"
#import <objc/runtime.h>

#define kPFUserSessionToken @"_sessionToken"
#define kPFUserIsNew @"_isNew"
#define kPFUserUserName @"_userName"
#define kPFUserEmail @"_email"
#define kPFUserPassword @"_password"

#define kPFUserData @"data"

@implementation PFUser (NSCoding)

- (void)encodeWithCoder:(NSCoder*)encoder
{
    // TODO:implement
    /*
	[encoder encodeObject:self.sessionToken forKey:kPFUserSessionToken];
    [encoder encodeObject:self.isNew forKey:kPFUserIsNew];
    [encoder encodeObject:self.username forKey:kPFUserUserName];
    [encoder encodeObject:self.password forKey:kPFUserPassword];
    [encoder encodeObject:self.email forKey:kPFUserEmail];
    
	if (self.isDataAvailable) {
		[encoder encodeObject:[self getData] forKey:kPFUserData];
	}
     */
    
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    // TODO:implement
    /*
    NSString *sessionToken = [aDecoder decodeObjectForKey:kPFUserSessionToken];
    bool isNew = [aDecoder decodeObjectForKey:kPFUserIsNew];
    NSString *username = [aDecoder decodeObjectForKey:kPFUserUserName];
    NSString *password = [aDecoder decodeObjectForKey:kPFUserPassword];
    NSString *email = [aDecoder decodeObjectForKey:kPFUserPassword];
    
	self = [PFFile fileWithName:name data:data];
	if (self) {
        [self setValue:url forKey:@"_url"];
	}
    */
	return self;
}

@end
